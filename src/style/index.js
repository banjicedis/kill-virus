import styled, { css, keyframes } from 'styled-components';
const rotate = keyframes`
0%{transform: rotate(90deg)},
100%{transform: rotate(360deg)}
`;


export const RelativeLayout = styled.div`
  width: 100vw;
  height: 500px;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  overflow: hidden;
  h1 {
    color: white;
  }
  h2{
    text-align: center;
    color: white;
    padding: 0 10px;
    font-size: 17px;
  }
`;
export const ImageLayout = styled.div`
  width: 300px;
  height: 400px;
  position: relative;
  overflow: hidden;
`;
export const Bakterija = styled.div`
 
  ${(props) => props.normalStyle && `${props.normalStyle}`}

  ${(props) =>
    props.rotate &&
    css`
      // animation: ${rotate} 1s ease-in;
     
    `};
    ${(props) => props.toCenter && `${props.toCenterStyle}`}
    ${(props) => props.animate && "transition: 2s ease all;"}
`;
export const Winner = styled.p`
    position: absolute;
    background: white;
    padding: 5px;
    font-size: 20px;
    border-radius: 8px;
    top: 50%;
    width: 80%;
    left: 10%;
    text-align: center;
    z-index: 5;
    margin: 0 auto;
    box-shadow: 0 1px 7px #0000004d;

`;
export const BumEfekat = styled.div`
  width: 30px;
  height: 30px;
  transform: scale(0);
  background: white;
  z-index:2;
  position: absolute;
  top: calc(50% - 15px); 
  right: calc(50% - 15px);
  ${(props) => props.animate && "transition: 0.3s ease all;"}
  ${(props) => props.grow && "transform: scale(1); "}

`;
export const Vakcina = styled.div`
${(props) => props.normalStyle && `${props.normalStyle}`}
  ${(props) => props.animate && "transition: 2s ease all;"}
    ${(props) => props.toCenter && `${props.toCenterStyle}`}
   
`;

