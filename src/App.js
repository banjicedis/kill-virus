import React from 'react';
import './App.css';
import Screen1 from './screens/Screen1';

const App = () => {
  return (
    <Screen1 />
  );
}

export default App;
