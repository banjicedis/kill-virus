import React, {useState } from 'react'
import { RelativeLayout, Vakcina, ImageLayout, Bakterija, Winner } from '../style'
import Lottie from 'react-lottie'
import BacteryImage from '../images/corona-v2.png';
import HelpImage from '../images/help-image.png'; 
import BabySuperman from '../images/baby-superman.png'; 
import BabyVacine from '../images/baby.png'; 

import vacineData from '../images/vacine-lottie.json'
import virusData from '../images/virus-lottie.json'

const defaultOptions = {
  loop: false,
  autoplay: false,
  animationData: vacineData,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice'
  }
};

const virusOptions = {
  loop: true,
  autoplay: true,
  animationData: virusData,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice'
  }
};

const objToCss = (whichStyle) => {
  const css = {
     ...whichStyle
}
const styleString = (
  Object.entries(css).reduce((styleString, [propName, propValue]) => {
    return `${styleString}${propName.replace(/[A-Z]/g, match => `-${match.toLowerCase()}`)}:${propValue};`;
  }, '')
);
return styleString;
}
const Screen1 = () => {
  const [toCenter, setToCenter] = useState(false);
  const [activeScreen, setActiveScreen] = useState(0);
  const [displayText, setDisplayText] = useState(false);
  const [winner, setWinner] = useState(false);
  const [playVaccine, setPlayVaccine] = useState(false);
  const attack = () => {
    setTimeout(() => setPlayVaccine(true), 2000);
    setToCenter(true,setTimeout(() => setDisplayText(true, setWinner(true, setTimeout(() => setActiveScreen(2, setToCenter(false, setDisplayText(false))), 3000))), 2500)
    )
  }
  const lose = () => {
    setToCenter(true,setTimeout(() => setDisplayText(true, setWinner(false, setTimeout(() => setActiveScreen(0, setToCenter(false, setDisplayText(false))), 3000))), 2500)
    )
  }
  const showBacktery = () => {
    setToCenter(true, setTimeout(() => setDisplayText(true, setTimeout(() => setActiveScreen(0, setDisplayText(false), setToCenter(false)), 3000) ), 2500))
  }
  const goToYourVacine = () => {
    setDisplayText(true);
  }
  const buttons = [
    {button1: 'Next',button2: false,  onButton1: () => {setActiveScreen(1)}},
    {button1: 'Vaccine', button2: 'Virus', onButton1: attack, onButton2: lose},
    {button1: 'Yes i want vaccine', button2: 'No, I dont want vacinne', onButton2: showBacktery, onButton1: () => {setActiveScreen(3)}},
    {button1: "Now I'm really sure I want the vaccine", button2: "I don't think the vaccine is good", onButton2: showBacktery, onButton1: () => {setActiveScreen(4)}},
    {button1: "Finish", button2: false, onButton2: showBacktery, onButton1: goToYourVacine},
  ]
  const EmojiLotie = <lottie-player src="https://assets2.lottiefiles.com/packages/lf20_nMuEto.json"  background="transparent"  speed="1"  style={{width: '240px', height: '240px'}}  loop autoplay></lottie-player>
  const VacineAnimation = <lottie-player src="https://assets4.lottiefiles.com/temp/lf20_ksLTeM.json"  background="transparent"  speed="1"   style={{width: "100px", height: "100px" }} loop  autoplay></lottie-player>;

  return ( 
    <div style={{display: 'flex', justifyContent: 'space-between', flexDirection: 'column', height: '80vh'}}>
    {activeScreen === 0 &&
  <RelativeLayout>
    <h1>Welcome</h1>
      <ImageLayout>
        <Bakterija normalStyle={objToCss({width: '240px', height: '240px', position: 'absolute', bottom: 'calc(50% - 120px)', left: 'calc(50% - 120px)'})} >
          {EmojiLotie}
        </Bakterija>
        <Vakcina  normalStyle={objToCss({width: '60px', height: '60px', position: 'absolute', top: '22px', left: '201px'})} >
          {VacineAnimation}
        </Vakcina>
      </ImageLayout>
      <h2>Throught this game you'll find out why the vaccine is good</h2>
  </RelativeLayout>
}
{activeScreen === 1 &&
  <RelativeLayout>
      <ImageLayout>
      <Bakterija normalStyle={objToCss({width: '240px', height: '240px', position: 'absolute', bottom: '10px', left: '10px'})} rotate={toCenter} toCenter={toCenter} animate={true} toCenterStyle={objToCss({left: 'calc(50% - 120px)', bottom: 'calc(50% - 120px)'})}>
          <Lottie options={virusOptions}
              height={240}
              width={240}

        />
        </Bakterija>
        <Vakcina  normalStyle={objToCss({width: '60px', height: '60px', position: 'absolute', top: '10px', right: '30px'})} rotate={toCenter} toCenter={toCenter} animate={true} toCenterStyle={objToCss({top: '80px', right: '98px'})} >
          <Lottie options={defaultOptions}
              height={100}
              width={100}
              isStopped={!playVaccine}
              isPaused={!playVaccine}
        />
        </Vakcina>
        {displayText && <Winner>{winner ? 'Vaccine Winner' : 'Game Over'}</Winner>}
      </ImageLayout>
      <h2>Thanks to vaccines, doctors have saved over 21 million lives</h2>
  </RelativeLayout>
}
{activeScreen === 2 &&
  <RelativeLayout>
      <ImageLayout>
      <Bakterija normalStyle={objToCss({width: '240px', height: '240px',zIndex: 5, position: 'absolute', bottom: '-250px', left: 'calc(50% - 120px)', opacity: 0})}  toCenter={toCenter} animate={true} toCenterStyle={objToCss({opacity: 1, bottom: 'calc(50% - 120px)',})}>
      <Lottie options={virusOptions}
              height={240}
              width={240}

        />
        </Bakterija>
        <Vakcina  normalStyle={objToCss({width: '240px', height: '240px', position: 'absolute', top: 'calc(50% - 120px)', right: 'calc(50% - 120px)', opacity: 1})} toCenter={toCenter} animate={true} toCenterStyle={objToCss({opacity: 0})} >
          <img width="100%" src={HelpImage} alt='bakterija' />
        </Vakcina>
        {displayText && <Winner>{winner ? 'Game Over' : 'Game Over'}</Winner>}
      </ImageLayout>
      <h2>{'The vaccine help your body. \n Your body is strong because of vaccine. \n Vaccine build your immunity'}</h2>
  </RelativeLayout>
}
{activeScreen === 3 &&
  <RelativeLayout>
      <ImageLayout>
      <Bakterija normalStyle={objToCss({width: '240px', height: '240px',zIndex: 5, position: 'absolute', bottom: '-250px', left: 'calc(50% - 120px)', opacity: 0})}  toCenter={toCenter} animate={true} toCenterStyle={objToCss({opacity: 1, bottom: 'calc(50% - 120px)',})}>
          <Lottie options={virusOptions}
              height={240}
              width={240}

        />
        </Bakterija>
        <Vakcina  normalStyle={objToCss({width: '240px', height: '240px', position: 'absolute',opacity: 1, top: 'calc(50% - 120px)', right: 'calc(50% - 120px)'})} toCenter={toCenter} animate={true} toCenterStyle={objToCss({opacity: 0})} >
          <img width="100%" src={BabyVacine} alt='bakterija' />
        </Vakcina>
        <Vakcina  normalStyle={objToCss({width: '240px', height: '240px', position: 'absolute',opacity: 1, top: '205px', right: '-112px'})} toCenter={toCenter} animate={true} toCenterStyle={objToCss({opacity: 0})} >
          {VacineAnimation}
        </Vakcina>
        {displayText && <Winner>{winner ? 'Game Over' : 'Game Over'}</Winner>}
      </ImageLayout>
      <h2>{'Not all vaccines are administereed by a injection some can be takenorally.So far vaccines have eradicated two diseases. '}</h2>
  </RelativeLayout>
}
{activeScreen === 4 &&
  <RelativeLayout>
      <ImageLayout>
      <Bakterija normalStyle={objToCss({width: '240px', height: '240px',zIndex: 5, position: 'absolute', bottom: '-250px', left: 'calc(50% - 120px)', opacity: 0})}  toCenter={toCenter} animate={true} toCenterStyle={objToCss({opacity: 1, bottom: 'calc(50% - 120px)',})}>
          <img width="100%" src={BacteryImage} alt='bakterija' />
        </Bakterija>
        <Vakcina  normalStyle={objToCss({width: '240px', height: '240px', position: 'absolute',opacity: 1, top: 'calc(50% - 120px)', right: 'calc(50% - 120px)'})} toCenter={toCenter} animate={true} toCenterStyle={objToCss({opacity: 0})} >
          <img width="100%" src={BabySuperman} alt='bakterija' />
        </Vakcina>
        {displayText && <Winner>{winner ? "Now it's time for you to go and get your vaccine" : 'Game Over'}</Winner>}
      </ImageLayout>
      <h2>{'Get vaccines and join me in saving our planet'}</h2>
  </RelativeLayout>
}
    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
       {buttons[activeScreen].button1 ?  <button className="first" disabled={toCenter} onClick={buttons[activeScreen].onButton1} >{buttons[activeScreen].button1 }</button> : null}
       {buttons[activeScreen].button2 ?  <button className="second" disabled={toCenter} onClick={buttons[activeScreen].onButton2} >{buttons[activeScreen].button2 }</button> : null}

     </div>
  </div>
  )
}

export default Screen1;